from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from meetimesblog import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('create/', views.post_create, name='post_create'),
    path('post/<int:post_id>/', views.post, name='post'),
    path('post/<int:post_id>/update', views.post_update, name='post_update'),
    path('post/<int:post_id>/delete', views.post_delete, name='post_delete'),
    path('add-category/', views.AddCategoryView.as_view(), name='add_category'),
    path('category/<str:cats>/', views.CategoryView, name='category'),
    path('search/', views.search, name='search'),
    path('tinymce/', include('tinymce.urls')),
    path('accounts/', include('allauth.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



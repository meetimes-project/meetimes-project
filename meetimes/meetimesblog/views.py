from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import CreateView

from .forms import CommentForm, PostForm
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



def get_author(user):
    qs = Author.objects.filter(user=user)
    if qs.exists():
        return qs[0]
    return None

def search(request):
    queryset = Post.objects.all()
    query = request.GET.get('q')
    if query:
        queryset = queryset.filter(
            Q(title__icontains=query)|
            Q(description__icontains=query)
        ).distinct()
    context = {
        'queryset': queryset,
    }
    return render(request, 'search_result.html', context)

def index(request):
    categories = Category.objects.all()
    queryset = Post.objects.all()
    paginator = Paginator(queryset, 4)
    page_request_var = 'page'
    page = request.GET.get(page_request_var, 1)
    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1)
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)
    context = {
        'categories': categories,
        'queryset': paginated_queryset,
        'page_request_var': page_request_var
    }
    return render(request, 'index.html', context)

def post(request, post_id):
    categories = Category.objects.all()
    post = get_object_or_404(Post, id=post_id)
    form = CommentForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.instance.user = request.user
            form.instance.post = post
            form.save()
            return redirect('post', post_id=post_id)
    context = {
        'categories': categories,
        'post': post,
        'form': form
    }
    return render (request, 'post.html', context)

def post_create(request):
    title = 'Create'
    form = PostForm(request.POST or None, request.FILES or None)
    author = get_author(request.user)
    if request.method == "POST":
        if form.is_valid():
            form.instance.author = author
            form.save()
            return redirect(reverse('post', kwargs={
                'post_id': form.instance.id
            }))
    context = {
        'title': title,
        'form': form
    }
    return render(request, 'post_create.html', context)

def post_update(request, post_id):
    title = 'Update'
    post = get_object_or_404(Post, id=post_id)
    form = PostForm(request.POST or None, request.FILES or None, instance=post)
    author = get_author(request.user)
    if request.method == "POST":
        if form.is_valid():
            form.instance.author = author
            form.save()
            return redirect(reverse('post', kwargs={
                'post_id': form.instance.id

            }))
    context = {
        'title': title,
        'form': form
    }
    return render(request, 'post_create.html', context)

def post_delete(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    post.delete()
    return redirect('index')


def CategoryView(request, cats):
    category_post = Post.objects.filter(categories__title__contains=cats)
    queryset = Category.objects.all()
    paginator = Paginator(queryset, 4)
    page_request_var = 'page'
    page = request.GET.get(page_request_var, 1)
    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1)
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)
    context = {
        'cats': cats,
        'category_post': category_post,
        'queryset': paginated_queryset,
        'page_request_var': page_request_var
    }
    return render(request, 'categories.html', context)

class AddCategoryView(CreateView):
    model = Category
    template_name = 'add_category.html'
    fields = '__all__'




